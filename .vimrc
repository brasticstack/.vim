"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible

" Use the following command to clone plugin repos:
" git submodule update --init --recursive

execute pathogen#infect()
execute pathogen#helptags()

set belloff=all
set encoding=utf8

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Default Indentation + Tabbing
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set autoindent
set smartindent

" Use Python-style tabbing.
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smarttab
set shiftround
set history=1000

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => filetype-specific mojo.
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype on
filetype plugin on
filetype indent on
syntax on

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" No sound on errors
set noerrorbells
set novisualbell
set t_vb=

set fileformat=unix
set backspace=2     " Backspace over anything in insert mode.
set timeoutlen=500
set scrolloff=3
set ruler
set number
set hidden
set lazyredraw      " Don't redraw while executing macros.
set magic
set showmatch
set matchtime=2
set wildmenu
set wildignore=*.pyc,*.o

set whichwrap+=<,>,h,l
set nowrap

" hlsearch: Space bar clears highlights.
set hlsearch
:nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Zenburn color scheme
:let g:zenburn_high_Contrast=1
:let g:less_ugly_Visual=1
colorscheme zenburn

set mouse=a
if has("gui_running")
    set mousehide
    set nomousefocus

    "Hide toolbar in gvim
    " set guioptions=aegit
    set guioptions=aegt

    set guifont=Droid\ Sans\ Mono:h12,Monaco:h12,
        \Bitstream\ Vera\ Sans\ Mono:h12

    if has("gui_gtk2")
        " This is a really good looking font!
        set guifont=DejaVu\ Sans\ Mono|10
    endif
    if has("gui_gtk3")
        " This is a really good looking font!
        set guifont=DejaVu\ Sans\ Mono\ Book|10
    endif
endif

if has("mouse_sgr")
    set ttymouse=sgr
endif


if has('transparency')
    set transparency=3
endif

set colorcolumn=80
highlight ColorColumn ctermbg=8 guibg=DarkGray
if has('gui_macvim') || has('gui_mac')
    highlight ColorColumn guibg=Gray17
endif

" Set shell for *nixes (has 'unix' includes linuxes)
if has('unix')
    set shell=/bin/bash
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" These options are great for a local machine, but could cause heartbreak if
" this rc is used on a server (say while editing configs.) These should be
" used in the machine-specific override file instead, where desired.
" set nobackup
" set nowritebackup
" set noswapfile

if has('persistent_undo')
    "Persistent undo (vim 7.3 and up)
    if has('unix')
        set undodir=~/.vim/undodir
    elseif has('win32') || has('win64') || has('win16')
        set undodir=C:\vimtemp\
    elseif has('dos32') || has('dos16')
        set undodir=C:\vimtemp\
    endif

    set undofile
endif

if !exists(':Erc')
    command Erc :e $MYVIMRC
endif

" FNL: Fix newline! Remove windows \n from \r newlines.
if !exists(':FNL')
    command FNL %s//\r/g
endif

" FNL: Remove whitespace from line endings.
if !exists(':FEOL')
    command FEOL %s_\s\+$__
endif
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Custom key mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Movement
" Treat long wrapped lines as break lines when attempting to move up or down.
map j gj
map k gk

" Tabs
map th :tabnext<CR>
map tl :tabprev<CR>
map tn :tabnew<CR>
map td :tabclose<CR>

" Easier switching of splits.
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Plugin specific mappings
map <F2> :BufExplorer<CR>
"
map <F8> :source $MYVIMRC<CR> :echo "vimrc reloaded" <CR>

" Fix xterm-keys navigation (particularly for tmux, which can send xterm-style
" keys, but sets &term to screen*.)
if &term =~ '^screen'
    execute "set <xUp>=\e[1;*A"
    execute "set <xDown>=\e[1;*B"
    execute "set <xRight>=\e[1;*C"
    execute "set <xLeft>=\e[1;*D"
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Experimental!
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Return to last edit position when opening files.
" Borrowed from https://amix.dk/vim/vimrc.html
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
" Remember info about open buffers on close
set viminfo^=%
" Always show the status line
set laststatus=2

" Delete trailing white space on save, useful for Python and CoffeeScript ;)
func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugin config
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <F3> :NERDTreeToggle<CR>
let NERDTreeIgnore=['\.pyc$', '__pycache__', '\.git$', '\.ropeproject']
" NERDTree now sets VIM's pwd to the tree root whenever it's changed.
" (plays better with python-mode/rope in particular.)
let NERDTreeChDirMode=2

" python-mode config:
if has("completeopt")
    completeopt menu
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Things that need to happen last
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Machine specific overrides.
let $local_conf=$MYVIMRC."_local"
if filereadable($local_conf)
    source $local_conf
endif

